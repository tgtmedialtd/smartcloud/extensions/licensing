<!-- Mention "documentation" or "docs" in the MR title -->
<!-- For changing documentation location use the "Change documentation location" template -->

## What does this MR do?

<!-- Briefly describe what this MR is about. -->

## Related issues

<!-- Link related issues below. Insert the issue link or reference after the word "Closes" if merging this should automatically close it. -->

## Author's checklist
- [ ] Link docs to and from the higher-level index page, plus other related docs where helpful.
- [ ] Apply the ~documentation label.

## Review checklist

All reviewers can help ensure accuracy, clarity, completeness.

**1. Primary Reviewer**

* [ ] Review by a code reviewer or other selected colleague to confirm accuracy, clarity, and completeness. This can be skipped for minor fixes without substantive content changes.

**2. Technical Writer**

* [ ] Optional: Technical writer review. If not requested for this MR, must be scheduled post-merge.

**3. Maintainer**

1. [ ] Review by assigned maintainer, who can always request/require the above reviews. Maintainer's review can occur before or after a technical writer review.
1. [ ] Ensure a release milestone is set.

/label ~documentation
